# TimeZoneWidget #

Show multiple timezones on your iOS device by dragging down the top/today menu.

### Widget in the Today menu ###

![widget.jpg](https://bitbucket.org/repo/e6eERn/images/2674526476-widget.jpg)

### Main application / Config ###

![config.jpg](https://bitbucket.org/repo/e6eERn/images/3364951154-config.jpg)

### Background ###

Two things led to this app:

1. I woke up one morning and decided that I wanted to learn a bit about Swift and iOS development.
2. I work with multiple teams in multiple timezones and have friends and family in a few different locations, so having easy access to current time in a few of these locations makes my day better.


### Heads up ###

Not only is this my first useful iOS application, I'm at best an amateur developer... 

While the application now does the job for me I do recognize that there are plenty of improvement areas that I just don't have time for (or want to prioritize) right now, but stay tuned - I may just get around to putting some more effort in.

Meanwhile I'd very much appreciate feedback, improvements and fixes.


### TODO: Known improvements, ideas and next steps ###

*In no particular order.*

* General code improvements
* The app has been specifically designed to run on my iPhone 6, but simulator shows no issues on iPhone 4/5
* Icon - Yes, it really needs an icon
* Timezone string could to look better
* Add a search field so users can search for the timezone they want
* UI/UX needs improvement
* iWatch - I don't have one, but it'd be cool to add support for it
* Learn about tests in Swift and XCode
* There's a known sorting issue under some circumstances of the selected timezones


### Thanks ###

* https://github.com/github/gitignore